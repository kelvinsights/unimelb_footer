<?php
/**
 * @file
 * unimelb_footer.bean.inc
 */

/**
 * Implements hook_bean_admin_ui_types().
 */
function unimelb_footer_bean_admin_ui_types() {
  $export = array();

  $bean_type = new stdClass();
  $bean_type->disabled = FALSE; /* Edit this to true to make a default bean_type disabled initially */
  $bean_type->api_version = 5;
  $bean_type->name = 'unimelb_footer';
  $bean_type->label = 'Unimelb Footer';
  $bean_type->options = '';
  $bean_type->description = '';
  $export['unimelb_footer'] = $bean_type;

  return $export;
}
