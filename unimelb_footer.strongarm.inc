<?php
/**
 * @file
 * unimelb_footer.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function unimelb_footer_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'date_format_site_date';
  $strongarm->value = 'j M Y - H:i';
  $export['date_format_site_date'] = $strongarm;

  return $export;
}
