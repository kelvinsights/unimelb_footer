<?php
/**
 * @file
 * unimelb_footer.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function unimelb_footer_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'unimelb_footer';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '~admin/*' => '~admin/*',
      ),
    ),
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'bean-unimelb-footer' => array(
          'module' => 'bean',
          'delta' => 'unimelb-footer',
          'region' => 'bottom',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;
  $export['unimelb_footer'] = $context;

  return $export;
}
