<?php
/**
 * @file
 * unimelb_footer.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function unimelb_footer_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'view any unimelb_footer bean'.
  $permissions['view any unimelb_footer bean'] = array(
    'name' => 'view any unimelb_footer bean',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'bean',
  );

  return $permissions;
}
