<?php
/**
 * @file
 * unimelb_footer.features.uuid_bean.inc
 */

/**
 * Implements hook_uuid_features_default_beans().
 */
function unimelb_footer_uuid_features_default_beans() {
  $beans = array();

  $beans[] = array(
    'label' => 'Unimelb Footer',
    'description' => NULL,
    'title' => 'The University of Melbourne',
    'type' => 'unimelb_footer',
    'data' => array(
      'view_mode' => 'default',
    ),
    'delta' => 'unimelb-footer',
    'view_mode' => 'default',
    'created' => 1372857002,
    'log' => '',
    'uid' => 0,
    'default_revision' => 1,
    'revisions' => array(),
    'vuuid' => 'edbda18a-678e-480d-bf04-27d2ecd90aea',
    'uuid' => '6ffaf437-8d5e-46e0-908f-28fb52cb0f7b',
    'field_pane1' => array(
      'und' => array(
        0 => array(
          'value' => '<p><span style="line-height: 1.6em;">The University of Melbourne</span><br />
Parkville 3010 VIC Australia</p>
',
          'format' => 'rich_text',
          'safe_value' => '<p><span style="line-height: 1.6em;">The University of Melbourne</span><br />
Parkville 3010 VIC Australia</p>
',
        ),
      ),
    ),
    'field_pane2' => array(
      'und' => array(
        0 => array(
          'value' => '<p><strong>Phone:</strong> +(61 3) 9035 5511<br />
<strong>Email:</strong> 13MELB@unimelb.edu.au</p>
',
          'format' => 'rich_text',
          'safe_value' => '<p><strong>Phone:</strong> +(61 3) 9035 5511<br /><strong>Email:</strong> <a href="mailto:13MELB@unimelb.edu.au">13MELB@unimelb.edu.au</a></p>
',
        ),
      ),
    ),
    'field_pane3' => array(
      'und' => array(
        0 => array(
          'value' => '<p><strong>Authoriser:</strong>&nbsp;Director, University Marketing &amp; Principal Web Officer, ITS<br />
<strong>Maintainer:</strong> Web Marketing Team, Marketing</p>
',
          'format' => 'rich_text',
          'safe_value' => '<p><strong>Authoriser:</strong> Director, University Marketing &amp; Principal Web Officer, ITS<br /><strong>Maintainer:</strong> Web Marketing Team, Marketing</p>
',
        ),
      ),
    ),
    'field_pane4' => array(
      'und' => array(
        0 => array(
          'value' => '<p><strong>Date created:</strong> [date_created]<br />
<strong>Last modified:</strong> [last_modified]</p>
',
          'format' => 'rich_text',
          'safe_value' => '<p><strong>Date created:</strong> [date_created]<br /><strong>Last modified:</strong> [last_modified]</p>
',
        ),
      ),
    ),
    'field_site_created' => array(
      'und' => array(
        0 => array(
          'value' => '2013-07-03 00:00:00',
          'timezone' => 'Australia/Melbourne',
          'timezone_db' => 'Australia/Melbourne',
          'date_type' => 'datetime',
        ),
      ),
    ),
  );
  return $beans;
}
